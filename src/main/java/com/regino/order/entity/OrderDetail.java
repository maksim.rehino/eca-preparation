package com.regino.order.entity;


import lombok.*;

import javax.persistence.*;

import static javax.persistence.FetchType.EAGER;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "order_details")
public class OrderDetail {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "description")
    private String description;

    @ManyToOne(fetch = EAGER)
    @JoinColumn(name = "order_id")
    private Order order;
}
