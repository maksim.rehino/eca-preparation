package com.regino.order.entity;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum Currency {
    USD("$"),
    BYN("BR");

    private final String symbol;

    @JsonValue
    public String getSymbol() {
        return symbol;
    }
}
