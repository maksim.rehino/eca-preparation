package com.regino.order.entity;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.EnumType.STRING;
import static javax.persistence.FetchType.LAZY;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "orders")
public class Order {
    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "total")
    private BigDecimal total;

    @Column(name = "currency")
    @Enumerated(STRING)
    private Currency currency;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(cascade = ALL, fetch = LAZY)
    @JoinColumn(name = "order_id")
    private List<OrderDetail> details;
}

