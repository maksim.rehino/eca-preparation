package com.regino.order.converter;

import com.regino.order.entity.Order;
import com.regino.order.web.controller.preview.OrderPreview;
import com.regino.order.web.dto.OrderDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collection;
import java.util.List;

import static java.util.stream.Collectors.toList;

@RequiredArgsConstructor
@Component
public class OrderConverter {

    private final OrderDetailsConverter orderDetailsConverter;

    public OrderDto toModel(Order entity) {
        return new OrderDto(
                entity.getId(),
                entity.getTotal().setScale(2, RoundingMode.HALF_UP).toPlainString(),
                entity.getCurrency(),
                orderDetailsConverter.toModels(entity.getDetails())
        );
    }

    public List<OrderDto> toModels(Collection<Order> orders) {
        return orders.stream()
                .map(this::toModel)
                .collect(toList());
    }

    public Order toEntity(OrderDto newOrder) {
        return Order.builder()
                .currency(newOrder.getCurrency())
                .total(new BigDecimal(newOrder.getTotal()))
                .details(orderDetailsConverter.toEntities(newOrder.getDetails()))
                .build();
    }

    public OrderPreview toPreview(OrderDto order) {
        return new OrderPreview(
                order.getId(),
                order.getTotal(),
                order.getCurrency()
        );
    }
}
