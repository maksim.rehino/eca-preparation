package com.regino.order.converter;

import com.regino.order.entity.OrderDetail;
import com.regino.order.repository.OrderRepository;
import com.regino.order.web.dto.OrderDetailsDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Component
@RequiredArgsConstructor
public class OrderDetailsConverter {

    private final OrderRepository orderRepository;

    public OrderDetailsDto toModel(OrderDetail detail) {
        return new OrderDetailsDto(
                detail.getId(),
                detail.getOrder() == null ? null : detail.getOrder().getId(),
                detail.getDescription()
        );
    }

    public List<OrderDetailsDto> toModels(Collection<OrderDetail> details) {
        return details.stream()
                .map(this::toModel)
                .collect(toList());
    }

    public OrderDetail toEntity(OrderDetailsDto detailsDto) {
        return OrderDetail.builder()
                .id(detailsDto.getId())
                .order(detailsDto.getOrderId() == null ? null : orderRepository.getById(detailsDto.getOrderId()))
                .description(detailsDto.getDescription())
                .build();
    }

    public List<OrderDetail> toEntities(Collection<OrderDetailsDto> details) {
        return details.stream()
                .map(this::toEntity)
                .collect(toList());
    }
}
