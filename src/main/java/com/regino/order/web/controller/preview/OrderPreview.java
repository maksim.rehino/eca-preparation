package com.regino.order.web.controller.preview;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.regino.order.entity.Currency;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@Data
@Builder
@RequiredArgsConstructor
@JsonInclude(NON_NULL)
public class OrderPreview {
    private final Long id;
    private final String total;
    private final Currency currency;
}
