package com.regino.order.web.controller;

import com.regino.order.web.dto.OrderDetailsDto;
import com.regino.order.web.service.OrderDetailsService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.NoSuchElementException;

@RequiredArgsConstructor
@RestController
@RequestMapping("/details")
public class OrderDetailsController {

    private final OrderDetailsService orderDetailsService;

    @GetMapping
    public Collection<OrderDetailsDto> findAll() {
        return orderDetailsService.findAll();
    }

    @GetMapping("/{id}")
    public OrderDetailsDto findById(@PathVariable long id) {
        return orderDetailsService.findById(id).orElseThrow(NoSuchElementException::new);
    }

    @PostMapping
    public OrderDetailsDto create(@RequestBody OrderDetailsDto newDetail) {
        return orderDetailsService.create(newDetail);
    }
}
