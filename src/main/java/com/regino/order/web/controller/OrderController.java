package com.regino.order.web.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.regino.order.config.RabbitMqConfig;
import com.regino.order.converter.OrderConverter;
import com.regino.order.web.dto.OrderDto;
import com.regino.order.web.service.OrderService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.NoSuchElementException;

@RequiredArgsConstructor
@Slf4j
@RestController
@RequestMapping("/orders")
public class OrderController {
    private final OrderService orderService;
    private final AmqpTemplate template;
    private final OrderConverter orderConverter;
    private final ObjectMapper mapper;

    @GetMapping
    public Collection<OrderDto> findAll() {
        return orderService.findAll();
    }

    @GetMapping("/{id}")
    public OrderDto findById(@PathVariable long id) {
        return orderService.findById(id).orElseThrow(NoSuchElementException::new);
    }

    @SneakyThrows
    @PostMapping
    public OrderDto create(@RequestBody OrderDto newOrder) {
        OrderDto savedOrder = orderService.create(newOrder);
        template.convertAndSend(RabbitMqConfig.EXCHANGE_NAME, "new-orders", mapper.writeValueAsString(orderConverter.toPreview(savedOrder)));
        log.info("created new order: {}", savedOrder);
        return savedOrder;
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id) {
        orderService.delete(id);
        template.convertAndSend(RabbitMqConfig.EXCHANGE_NAME, "orders-to-delete", id);
        log.info("deleted order with id: {}", id);
    }
}
