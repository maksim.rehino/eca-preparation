package com.regino.order.web.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.regino.order.entity.Currency;
import lombok.Builder;
import lombok.Data;

import java.util.Collections;
import java.util.List;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@Data
@Builder
@JsonInclude(NON_NULL)
public class OrderDto {

    private final Long id;

    private final String total;

    private final Currency currency;

    private final List<OrderDetailsDto> details;

    public OrderDto(Long id, String total, Currency currency, List<OrderDetailsDto> details) {
        this.id = id;
        this.total = total;
        this.currency = currency;
        this.details = details == null ? Collections.emptyList() : details;
    }
}
