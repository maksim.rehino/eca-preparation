package com.regino.order.web.dto;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@Data
@Builder
@RequiredArgsConstructor
@JsonInclude(NON_NULL)
public class OrderDetailsDto {
    private final Long id;
    private final Long orderId;
    private final String description;
}
