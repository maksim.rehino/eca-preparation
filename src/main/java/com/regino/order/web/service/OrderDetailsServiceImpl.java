package com.regino.order.web.service;

import com.regino.order.converter.OrderDetailsConverter;
import com.regino.order.entity.OrderDetail;
import com.regino.order.repository.OrderDetailsRepository;
import com.regino.order.web.dto.OrderDetailsDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class OrderDetailsServiceImpl implements OrderDetailsService {

    private final OrderDetailsRepository orderDetailsRepository;
    private final OrderDetailsConverter orderDetailsConverter;

    @Override
    public Collection<OrderDetailsDto> findAll() {
        return orderDetailsConverter.toModels(orderDetailsRepository.findAll());
    }

    @Override
    public Optional<OrderDetailsDto> findById(long id) {
        return orderDetailsRepository.findById(id).map(orderDetailsConverter::toModel);
    }

    @Override
    public OrderDetailsDto create(OrderDetailsDto newDetail) {
        OrderDetail entityToSave = orderDetailsConverter.toEntity(newDetail);
        OrderDetail savedEntity = orderDetailsRepository.save(entityToSave);
        return orderDetailsConverter.toModel(savedEntity);
    }
}
