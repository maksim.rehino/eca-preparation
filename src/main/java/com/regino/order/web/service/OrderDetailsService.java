package com.regino.order.web.service;

import com.regino.order.web.dto.OrderDetailsDto;

import java.util.Collection;
import java.util.Optional;

public interface OrderDetailsService {
    Collection<OrderDetailsDto> findAll();

    Optional<OrderDetailsDto> findById(long id);

    OrderDetailsDto create(OrderDetailsDto newDetail);
}
