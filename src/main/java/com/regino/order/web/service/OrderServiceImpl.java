package com.regino.order.web.service;

import com.regino.order.converter.OrderConverter;
import com.regino.order.entity.Order;
import com.regino.order.repository.OrderRepository;
import com.regino.order.web.dto.OrderDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;
    private final OrderConverter orderConverter;

    @Override
    @Transactional
    public Collection<OrderDto> findAll() {
        return orderConverter.toModels(orderRepository.findAll());
    }

    @Override
    @Transactional
    public Optional<OrderDto> findById(long id) {
        return orderRepository.findById(id)
                .map(orderConverter::toModel);
    }

    @Override
    public OrderDto create(OrderDto newOrder) {
        Order entityToSave = orderConverter.toEntity(newOrder);
        Order savedEntity = orderRepository.save(entityToSave);
        return orderConverter.toModel(savedEntity);
    }

    @Override
    public void delete(long id) {
        orderRepository.deleteById(id);
    }
}
