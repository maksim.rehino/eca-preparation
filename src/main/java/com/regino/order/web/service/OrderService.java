package com.regino.order.web.service;

import com.regino.order.web.dto.OrderDto;

import java.util.Collection;
import java.util.Optional;

public interface OrderService {
    Collection<OrderDto> findAll();

    Optional<OrderDto> findById(long id);

    OrderDto create(OrderDto newOrder);

    void delete(long id);
}
